import React from 'react';

export default function Home() {
    return (
        <div className='centering'>
            <h1>Sorting Algorithms</h1>
            <nav>
                <li><button>Bubble Sort</button></li>
                <li><button>Insertion Sort</button></li>
                <li><button>Merge Sort</button></li>
            </nav>
        </div>
    );
}
