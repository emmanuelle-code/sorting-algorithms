import React, {useContext} from 'react';
import { ConfigContext } from '../context/Config';

export default function Headbar() {
    const {elements, setelements} = useContext(ConfigContext);
    return (
        <header>
            <div>
                <img alt="logo" src="https://firebasestorage.googleapis.com/v0/b/emmanuellecode.appspot.com/o/logo.svg?alt=media&token=4a8198df-2038-4112-9111-46f84beece43" />
            </div>
            <div className='rigth'>
                <nav>
                    <li>
                        <input max={50} value={elements} onChange={e => setelements(e.target.value)} type="range" />
                    </li>
                    <li><button>Sort</button></li>
                </nav>
            </div>
        </header>
    );
}
