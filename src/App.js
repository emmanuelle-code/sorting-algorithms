import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Headbar from './components/headbar';
import Home from './components/home';
import ConfigProvider from './context/Config';

function App() {
  return (
    <BrowserRouter>
    <ConfigProvider>
      <Headbar />
        <Routes>
          <Route path='/' element={<Home />}/>
        </Routes>
    </ConfigProvider>
    </BrowserRouter>
  );
}

export default App;
