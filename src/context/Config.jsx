import React, {createContext, useState} from 'react';

export const ConfigContext = createContext();


const ConfigProvider = (props) => {
    const [elements, setelements] = useState(25);
    return (
        <ConfigContext.Provider
        value={{
            elements, setelements
        }}>
            {props.children}
        </ConfigContext.Provider>
    );
}

export default ConfigProvider;